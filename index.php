<html>
<head>
  <title>MLB Scraper</title>
</head>
<body>
    <?php
//ob_implicit_flush(true);

    function run_cmd_with_output($cmd){

        $descriptorspec = array(
       0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
       1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
       2 => array("pipe", "w")    // stderr is a pipe that the child will write to
       );
        flush();

//    ob_start();

        $process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());
        echo "<pre>";
        if (is_resource($process)) {
            while ($s = fgets($pipes[1])) {


                print $s;

//            ob_flush();
                flush();
            }
        }
        echo "</pre>";

//    ob_end_flush();
//

    }

//ini_set('display_startup_errors', 1);
//ini_set('display_errors', 1);
//error_reporting(-1);
//$cmd = 'source ./bin/activate && nice -n 16 python scrape_espn.py 2>&1';
//run_cmd_with_output($cmd);

    echo  "<h1> Scraped MLB Info: </h1>";

// LIST ALL THE FILES AND MAKE THEM DOWNLOADABLE

    if ($handle = opendir('./excel')) {

        while (false !== ($entry = readdir($handle))) {

            if ($entry != "." && $entry != "..") {

                echo "<a href='./excel/$entry' download> $entry </a><br>";
            }
        }

        closedir($handle);
    }

    ?>

    <div id='json-home'></div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/renderjson.js"></script>

    <script>
    // $.getJSON("todays_espn_game_data.json", function(json) {
    //     console.log(json); // this will show the info it in firebug console
    // });

    $(document).ready(function(){

        renderjson.set_show_to_level(3);
        renderjson.set_sort_objects(true);


        $.getJSON("todays_espn_game_data.json", function(data){
            console.log(data);
            document.getElementById("json-home").appendChild(renderjson(data));
        });


    });


    </script>

</body>
</html>

from pymongo import MongoClient
import pprint
import json
import copy

# ====================================================================================================

# mongodb complains when there are .'s in the keys, so replace them recursively.
# def walk_and_fix_keys(node):
#     for key, item in node.items():
#
#         if "." in key:
#             new_key = key.replace(".", "")
#             node[new_key] = item
#             del node[key]
#
#         if isinstance(item, dict):
#             return walk_and_fix_keys(item)
#
#     return node


# ====================================================================================================

client = MongoClient('localhost', 27017)
db = client.MLB
print(client)

def AddOneGamesFeaturesToMongoDB(game):

    games = db.games

    games.insert_one(game)


def AddOneFeaturesToMongoDB(feat):
    features = db.features
    features.insert_one(feat)

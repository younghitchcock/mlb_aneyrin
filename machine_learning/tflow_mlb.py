# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example code for TensorFlow Wide & Deep Tutorial using TF.Learn API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import tempfile

# from six.moves import urllib

import pandas as pd
import tensorflow as tf

CSV_FP = "csv/games_as_features.csv"
MODEL_DIR = "./model"

continuous_colnames = None
categorical_colnames = None
LABEL_COLNAME = None

def set_feature_and_label_colnames(df):

    global continuous_colnames, categorical_colnames, LABEL_COLNAME

    continuous_colnames = [x for x in df.columns if
                           ("float64" in str(df[x].dtype) or "int64" in str(df[x].dtype)) and "measure_" not in x]

    categorical_colnames = [x for x in df.columns if x not in continuous_colnames and "measure_" not in x]

    label_colnames = [x for x in df.columns if "measure_" in x]

    LABEL_COLNAME = "measure_winner_binary"

    print(label_colnames)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def create_feature_columns(df):

    # --------------- create X & Y tensors from these column -------------------

    #  ----------------------------- CONTINUOUS X -----------------------------

    # From Tensorflow tutorial!
    # Creates a dictionary mapping from each continuous feature column name (k) to
    # the values of that column stored in a constant Tensor.
    continuous_cols = {k: tf.constant(df[k].values) for k in continuous_colnames}

    # ----------------------------- CATEGORICAL X -----------------------------

    # Creates a dictionary mapping from each categorical feature column name (k)
    # # to the values of that column stored in a tf.SparseTensor.
    # categorical_cols = {
    #     k: tf.SparseTensor(
    #         indices=[[i, 0] for i in range(df[k].size)],
    #         values=df[k].values,
    #         dense_shape=[df[k].size, 1])
    #     for k in categorical_colnames}

    # Merges the two dictionaries into one.
    feature_cols = dict(continuous_cols)
    # feature_cols.update(categorical_cols)

    # --------------------------------- TRUE Y ---------------------------------

    #  Converts the label column into a constant Tensor.
    label = tf.constant(df[LABEL_COLNAME].values)

    return feature_cols, label

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def build_model():

    global continuous_colnames, categorical_colnames, LABEL_COLNAME

    deep_columns = []

    # JUST ADD THEM ALL !
    for c in continuous_colnames:
        deep_columns.append(tf.contrib.layers.real_valued_column(c))

    # TODO: add categorical columns

    m = tf.contrib.learn.DNNClassifier(model_dir=MODEL_DIR,
                                       feature_columns=deep_columns,
                                       hidden_units=[100, 50])

    # TODO: add optimiser!

    return m

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def train_and_eval(df):

    """
    Get just hte coluns you need!
    Drop NA or fix it up somehow!


    """

    print(df.shape)

    # df = df.dropna(how='any', axis=0)       # This is going to be the death of me

    print(df.shape)

    # df_test = df.sample(frac=0.1)
    #
    # df_train = df[~df.isin(df_test).all(1)]             # the training set is NOT IN the testing set
    #
    # print(df_test.shape, df_train.shape)

    #
    # m = build_model()  # build
    # m.fit(input_fn=lambda: create_feature_columns(df_train), steps=train_steps)  # fit
    # results = m.evaluate(input_fn=lambda: create_feature_columns(df_test), steps=1)  # evaluate


if __name__ == "__main__":

    data = pd.read_csv(CSV_FP)

    set_feature_and_label_colnames(data)

    build_model()

    train_and_eval(data)
    # feature_cols, label = create_feature_columns(data)

    # print(data.columns)



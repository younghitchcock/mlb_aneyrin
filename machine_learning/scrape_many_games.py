import os, datetime
import mlbgame
from pprint import pprint
from utils import merge_dicts
import add_games_to_mongodb as mdb
import json
import pandas as pd


'''
What stats do you want???

Average over batting

Get individual pitching

[
    result-metric
    home batting avg,
    home pitchers ...,
    away batting avg,
    away pitchers...
]

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

When learning. What is going to predict the next game?

X = last N games for a given team OR any team
Y = the win or loss of the N+1th game

Y;s need to convert

Layout is

{
    team 1:
    [
        {  last N games stats (X's) & this games stats (Y's)  },
    ],
    team 2:
    [
        ...
    ],
    etc...
}

Issue:
    is the use of the same data for both the winning & the losing team a factor??
    Think about this....

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Now we want to set up tensor flow. Split each vector into X's and true Y's and run the shit



'''

def GetBattingTotals(game):

    ''' BATTING STATS => team totals '''

    feature_dict = {}

    both_team_stats = mlbgame.team_stats(game.game_id)

    for side in [x for x in both_team_stats.keys() if "batting" in x]:      # choose team keys about batting
        team_stats = both_team_stats[side]
        for stat_name in [x for x in dir(team_stats) if x[0:2] != "__"]:    # choose keys that are not __xx__

            label = "{} {}".format(side, stat_name)
            value = getattr(team_stats, stat_name)

            feature_dict[label] = value

    return feature_dict

def GetIndividualPitchers(game):

    ''' PITCHER STATS => individual stats '''
    feature_dict = {}

    player_stats = mlbgame.player_stats(game.game_id)

    pitching_sides = [x for x in player_stats.keys() if "pitching" in x]

    MAX_PITCHERS = 4     # More than 4 pitchers appears to be a rarity & can probably be ignored!

    for side in pitching_sides:
        for i, pitcher in enumerate(player_stats[side]):

            if i > MAX_PITCHERS:
                break

            for stat in [x for x in dir(pitcher) if x[0:2] != "__" and "nice_output" not in x and "first_last" not in x]:
                label = "{} {} {}".format(side, i, stat)
                value = getattr(pitcher, stat)
                feature_dict[label] = value

    return feature_dict


def GetWinMetric(game):

    winner_binary = 1 if game.home_team_runs > game.away_team_runs else 0               # home win = 1. away win = 0
    winner_margin = game.home_team_runs - game.away_team_runs                          # categorical classifier?

    try:
        winner = game.w_team
    except:
        winner = "DRAW"

    return {
        "measure_winner": winner,
        "measure_winner_binary": winner_binary,
        "measure_winner_margin": winner_margin,
        "measure_home_team": game.home_team,
        "measure_away_team": game.away_team,
        "measure_home_runs": game.home_team_runs,
        "measure_away_runs": game.away_team_runs,
    }

def CreateFeatureVector(game):

    batting_features = GetBattingTotals(game)
    pitcher_features = GetIndividualPitchers(game)
    win_metric_features = GetWinMetric(game)


    return merge_dicts(win_metric_features, batting_features, pitcher_features)
    # return game_features

# ============================================================================================
# --  --  --  --  --  --  --  --            STEP 1          --  --  --  --  --  --  --  --  --
# ============================================================================================

def GetGameDataByTeam():

    games_by_team = {}

    N_DAYS_AGO = 100 # I week for now!

    for days_ago in range(2, N_DAYS_AGO):

        the_past = datetime.datetime.now() - datetime.timedelta(days=days_ago)

        print(the_past.date())

        for game in mlbgame.day(the_past.year, the_past.month, the_past.day):
            print("\t Creating a feature vector from:", game)

            try:
                g = CreateFeatureVector(game)

                for x in [game.home_team, game.away_team]:
                    if x not in games_by_team:
                        games_by_team[x] = []

                    games_by_team[x].append(g)  # OLDER GAMES @ THE BACK! Most recent games @ the front

            except Exception as e:
                print("* * * * * There was a problem with the last game: {}".format(e))
    # pprint(games_by_team)

    return games_by_team


# ============================================================================================
# --  --  --  --  --  --  --  --            STEP 2          --  --  --  --  --  --  --  --  --
# ============================================================================================

def CreateAnalysisUnits(data):

    '''
    Get as far as you can without needding the data in memory coz it takes time
    Once you need it, do a scrape once, and save it to a json flat file...
    '''
    N_GAME_WINDOW = 4

    games_as_features = {}

    for team, games in data.items():            # each team has a list of "featurised" games

        games_as_features[team] = []

        # for each game's result (Y), store the window's worth of previous games' stats as predictors (X)
        for i, game in enumerate(games):

            # once your window reaches the end of your game array, stop
            if i + N_GAME_WINDOW >= len(games)-1:
                continue

            # get the wins of the current game (TRUE Y's)
            wins_aka_true_Ys = {k: v for k, v in game.items() if "measure_" in k}

            # get the data for the N previous games (X's)
            n_previous_games = []

            for j in range(1, N_GAME_WINDOW+1):
                n_previous_games.append({"n-{} {}".format(j, k): v for k, v in games[i - j].items() if "measure_" not in k})

            # Store the combined X's and Y's
            games_as_features[team].append(merge_dicts({"_team": team}, wins_aka_true_Ys, *n_previous_games))


    return games_as_features


# ============================================================================================
# --  --  --  --  --  --  --  --            STEP x          --  --  --  --  --  --  --  --  --
# ============================================================================================
def DataDictToCSV(features, out_csv_path):

    '''
    features is a list of dicts   [ {}, {}, {}  ]
    Pandas can turn this into a csv with a one liner
    '''

    list_of_feature_dicts = []

    for team, features in features.items():
        list_of_feature_dicts += features       # One long list of shit
        print(team, len(features), type(features))
        try:
            print( type(features[0]), len(features[0]))
        except:
            print("* * * * *8 ")

    df = pd.DataFrame(list_of_feature_dicts)
    df.to_csv(out_csv_path)

# ============================================================================================
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ============================================================================================

if __name__ == "__main__":

    game_data_by_team = None
    games_as_features = None

    game_data_by_team_fn = 'json/game_data_by_team.json'                    # step 1
    # games_as_features_fn = 'json/games_as_features.json'                    # step 2
    csv_games_as_features_fn = 'csv/games_as_features.csv'                  # step 3

    if not os.path.isfile(game_data_by_team_fn):
        game_data_by_team = GetGameDataByTeam()
        with open(game_data_by_team_fn, 'w') as outfile:
            json.dump(game_data_by_team, outfile)
    else:
        print(game_data_by_team_fn, " Already Exists!!!")
        with open(game_data_by_team_fn, 'r') as fp:
            game_data_by_team = json.load(fp)

    #   STORING AS A JSON DICT IS INEFFICIENT!!
    # if not os.path.isfile(games_as_features_fn):
    #     games_as_features = CreateAnalysisUnits(game_data_by_team)
    #     with open(games_as_features_fn, 'w') as outfile:
    #         json.dump(games_as_features, outfile)
    # else:
    #     print(games_as_features_fn, " Already Exists!!!")
    #     with open(games_as_features_fn, 'r') as fp:
    #         games_as_features = json.load(fp)


    games_as_features = CreateAnalysisUnits(game_data_by_team)

    DataDictToCSV(games_as_features, csv_games_as_features_fn)

    # pprint(games_as_features)
    # mdb.AddOneFeaturesToMongoDB(games_as_features)
    # for team, features in games_as_features.items():
    #     mdb








    # for t in mlbgame.teams():
    #
    #     print(t)
    #     print([x for x in dir(t) if x[0:2] != "__"])

    # ''' MLB GAME already caches the data on disk somewhere'''
    #
    # for days_ago in range(365):
    #     datetime.datetime.now() - datetime.timedelta(days=1, years=1)
    #
    # day = mlbgame.day(2017, 8, 14, away="Indians")
    # print(day)
    # #
    # for d in day:
    #     feature_dict = CreateFeatureVector(d)
    # #
    #
    #         pprint(feature_dict)
    #         # mdb.AddOneGamesFeaturesToMongoDB(feature_dict)


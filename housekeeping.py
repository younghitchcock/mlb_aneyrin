import os
from datetime import datetime
import pytz
import re

def KeepTwoWeeksWorthOfFilesOnly(project_root):

    excel_path = os.path.join(project_root, "excel")

    now = datetime.today()

    for fn in os.listdir(excel_path):

        file = os.path.join(excel_path, fn)

        match = re.search(r'\d{4}-\d{2}-\d{2}', file)  # strip date out of the filename

        if match is not None:
            date = datetime.strptime(match.group(), '%Y-%m-%d')#.date()

            delta = now - date

            if delta.days > 15:
                try:
                    print("Time delta is: {}. removing file: ".format(delta.days), file)
                    os.remove(file)
                except:
                    pass
